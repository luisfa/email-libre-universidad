# Manifiesto contra las plataformas privativas de correo en la universidad.

En los últimos meses, muchas universidades han pasado su correo
electrónico corporativo a servicios comerciales alojados en la nube.
Por ejemplo en
[las universidades gallegas](https://twitter.com/gpul_/status/1153979399198466048) o
[en la universidad de Sevilla](https://twitter.com/skotperez/status/1149068916930621446). Esto
es un gran error, por las razones siguientes.

1. **El correo es un sistema crítico** hasta el punto que la dirección de
   correo suele servir como identificador personal del usuario. La
   externalización de un servicio crítico y esencial tal como este sería
   equivalente a que las clases de una universidad se impartieran por
   parte de una academia o una empresa privada.

1. **Pasar de servidores propios a servidores ajenos puede [incumplir la
   normativa europea](https://www.genbeta.com/seguridad/prohiben-usar-escuelas-alemanas-office-365-nubes-apple-google-problemas-privacidad)**,
   incluso aunque los servidores en la nube estén alojados dentro de
   la unión europea.

1. **Los sistemas de correo comercial en la nube
   [son casi siempre instrumentos de vigilancia masiva](https://www.gnu.org/proprietary/malware-google.html)**.
   Confiando la gestión de un servicio tan fundamental a empresas con prácticas opacas
   las universidades comprometen información personal del personal universitario,
   discusiones institucionales internas y avances en investigación que aún no se han
   hecho públicos.

1. El ámbito global de estos servicios de correo implica **riesgos imprevisibles para los miembros
   de la comunidad universitaria más allá de las fronteras españolas, e incluso europeas; y por
   un tiempo indefinido en el futuro**.
   ¿Puede un alumno de intercambio sufrir intromisiones en su intimidad o ser víctima de coacciones
   o de persecución política por parte de empresas o de su propio gobierno, una vez retornado a su
   país de origen, incluso años después de su etapa estudiando en España?
   ¿Puede un gobierno autoritario, o uno democrático pero con acceso privilegiado a los datos de las
   compañías que operan en su territorio, denegar el visado a un profesor que quiera visitar el país
   para participar en un congreso basándose en el contenido de su correspondencia dentro de la
   universidad?
